package com.threadjava.post;

import com.threadjava.comment.CommentRepository;
import com.threadjava.exceptions.IllegalPostAccessException;
import com.threadjava.post.dto.*;
import com.threadjava.post.model.Post;
import com.threadjava.postreactions.PostReactionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.threadjava.auth.TokenService.getUserId;

@Service
public class PostsService {

    @Autowired
    private PostsRepository postsCrudRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private PostReactionsRepository reactionsRepository;

    public List<PostListDto> getAllPosts(
            Integer from,
            Integer count,
            Boolean isOwnPosts,
            Boolean isSomeonesPosts,
            Boolean isLikedPosts)
    {
        var pageable = PageRequest.of(from / count, count);
        return postsCrudRepository
                .findAllPosts(null, pageable)
                .stream()
                .filter(post -> {
                    var result = true;
                    if (Boolean.TRUE.equals(isOwnPosts)) {
                        result = getUserId().equals(post.getUser().getId());
                    }
                    if (Boolean.TRUE.equals(isSomeonesPosts)) {
                        result &= !getUserId().equals(post.getUser().getId());
                    }
                    if (Boolean.TRUE.equals(isLikedPosts)) {
                        var reaction = reactionsRepository.getPostReaction(getUserId(), post.getId());
                        if (reaction.isPresent()) {
                            result &= reaction.get().getIsLike();
                        } else {
                            result = false;
                        }
                    }
                    return result;
                })
                .map(PostMapper.MAPPER::postListToPostListDto)
                .collect(Collectors.toList());
    }

    public PostDetailsDto getPostById(UUID id) {
        var post = postsCrudRepository.findPostById(id)
                .map(PostMapper.MAPPER::postToPostDetailsDto)
                .orElseThrow();

        var comments = commentRepository.findAllCommentsByPostId(id)
                .stream()
                .filter(comment -> !comment.isDeleted())
                .map(PostMapper.MAPPER::commentToCommentDto)
                .collect(Collectors.toList());
        post.setComments(comments);

        return post;
    }

    public PostCreationResponseDto create(PostCreationDto postDto) {
        var post = PostMapper.MAPPER.postDetailsDtoToPost(postDto);
        var postCreated = postsCrudRepository.save(post);
        return PostMapper.MAPPER.postToPostCreationResponseDto(postCreated);
    }

    public PostUpdateResponseDto update(PostUpdateDto postDto) {
        var postId = postDto.getPostId();

        var currentPost = getPostIfAccessAllowed(postId);
        var post = PostMapper.MAPPER.postUpdateDtoToPost(postDto);
        currentPost.setBody(post.getBody());
        currentPost.setImage(post.getImage());

        var postUpdated = postsCrudRepository.save(currentPost);
        return PostMapper.MAPPER.postToPostUpdateResponseDto(postUpdated);
    }

    public PostDeletedDto delete(UUID id) {
        var currentPost = getPostIfAccessAllowed(id);
        currentPost.setDeleted(true);
        var postDeleted = postsCrudRepository.save(currentPost);
        return PostMapper.MAPPER.postToPostDeleted(postDeleted);
    }

    private Post getPostIfAccessAllowed(UUID postId) {
        var post = postsCrudRepository.findById(postId).orElseThrow();

        var postOwner = post.getUser();
        if (!postOwner.getId().equals(getUserId())) {
            throw new IllegalPostAccessException("Current user cannot modify post");
        }

        return post;
    }
}
