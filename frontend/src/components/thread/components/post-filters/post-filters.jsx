import * as React from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { threadActionCreator } from 'src/store/actions';
import { Checkbox, Dropdown } from 'src/components/common/common';
import { IconName } from 'src/common/enums/enums';

import styles from './styles.module.scss';

const PostFilters = ({ postsFilter }) => {
  const [showOwnPosts, setShowOwnPosts] = React.useState(false);
  const [showSomeonesPosts, setShowSomeonesPosts] = React.useState(false);
  const [showLikedPosts, setShowLikedPosts] = React.useState(false);
  const dispatch = useDispatch();

  const showPosts = () => {
    postsFilter.from = 0;
    postsFilter.isOwnPosts = showOwnPosts;
    postsFilter.isSomeonesPosts = showSomeonesPosts;
    postsFilter.isLikedPosts = showLikedPosts;
    dispatch(threadActionCreator.loadPosts(postsFilter));
    postsFilter.from = postsFilter.count;
  };

  React.useEffect(showPosts, [showOwnPosts, showSomeonesPosts, showLikedPosts]);

  const filters = [
    {
      id: 1,
      label: 'Show only my posts',
      checked: showOwnPosts,
      onChange: checked => {
        setShowOwnPosts(checked);
        setShowSomeonesPosts(false);
      }
    },
    {
      id: 2,
      label: 'Show only someone\'s posts',
      checked: showSomeonesPosts,
      onChange: checked => {
        setShowSomeonesPosts(checked);
        setShowOwnPosts(false);
      }
    },
    {
      id: 3,
      label: 'Show only liked posts',
      checked: showLikedPosts,
      onChange: checked => setShowLikedPosts(checked)
    }
  ];

  const toggleSelectedFilters = (e, { label, checked }) => {
    const chosenFilter = filters.find(filter => filter.label === label);
    chosenFilter.onChange(checked);
  };

  return (
    <Dropdown
      text="Filter Posts"
      className={`icon ${styles.dropdown}`}
      icon={IconName.FILTER}
      item
      simple
      labeled
      button
    >
      <Dropdown.Menu>
        {filters.map(({ id, label, checked }) => (
          <Dropdown.Item key={id}>
            <Checkbox
              label={label}
              toggle
              checked={checked}
              onChange={toggleSelectedFilters}
            />
          </Dropdown.Item>
        ))}
      </Dropdown.Menu>
    </Dropdown>
  );
};

PostFilters.propTypes = {
  postsFilter: PropTypes.exact({
    isOwnPosts: PropTypes.bool.isRequired,
    isSomeonesPosts: PropTypes.bool.isRequired,
    isLikedPosts: PropTypes.bool.isRequired,
    from: PropTypes.number.isRequired,
    count: PropTypes.number.isRequired
  }).isRequired
};

export default PostFilters;
