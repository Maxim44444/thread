import * as React from 'react';
import PropTypes from 'prop-types';
import { ButtonType } from 'src/common/enums/enums';
import { Button, Form } from 'src/components/common/common';

const UpdateComment = ({ postId, commentId, onCommentUpdate, commentBody }) => {
  const [body, setBody] = React.useState(commentBody);

  const handleAddComment = async () => {
    if (!body) {
      return;
    }
    await onCommentUpdate({ postId, id: commentId, body });
  };

  return (
    <Form reply onSubmit={handleAddComment}>
      <Form.TextArea
        value={body}
        onChange={ev => setBody(ev.target.value)}
      />
      <Button type={ButtonType.SUBMIT} isPrimary>
        Update comment
      </Button>
    </Form>
  );
};

UpdateComment.propTypes = {
  commentBody: PropTypes.string.isRequired,
  postId: PropTypes.string.isRequired,
  commentId: PropTypes.string.isRequired,
  onCommentUpdate: PropTypes.func.isRequired
};

export default UpdateComment;
